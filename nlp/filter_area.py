from osgeo import ogr
import os

shapefile = "states.shp"
driver = ogr.GetDriverByName("ESRI Shapefile")
dataSource = driver.Open(shapefile, 0)
layer = dataSource.GetLayer()

top_left = "152.794758 -26.985915"
top_right = "152.800715 -26.986220"
bottom_right = "152.801550 -26.992257"
bottom_left = "152.793810 -26.992049"

wkt = f"POLYGON (({top_left},{top_right},{bottom_right},{bottom_left}))"
layer.SetSpatialFilter(ogr.CreateGeometryFromWkt(wkt))

for feature in layer:
    print(feature.GetField("STATE_NAME"))
