#https://readthedocs.org/projects/pyspellchecker/downloads/pdf/latest/

from spellchecker import SpellChecker
spell = SpellChecker()
# # find those words that may be misspelled
# words have been added to a dictionary:
# spell.word_frequency.load_words(['Water', 'Main', 'main', 'watermain', 'pvc', 'ac', 'ac', 'blue', 
# 'bruit', 'blue', 'brute', 'tapping', 'band', '100', '150', '200', '225', 
# 'trunk', '450', '300', 'line', 'seal', 'break', 'crack', 'radial', 'brocken', 'rhino',  
# 'Water', 'Service', 'service', 'ferrule', 'poly', 'polly', '20mm', 
# '50', '40', '32', 'main', 'to', 'meter', 'ferrel', 'ferule', '25mm', '20', 'mm', 
# '25', 'mm', 'conduit', 'line', 'split', 'service', 'main', 'ferral', 'main', 'to', 'water', 
# 'meter', 'broken', 'ferrule', '', 'Other', 'valve', 'hydrant', 'meter', 'box', 
# 'flush', 'stopcock', 'stop', 'cock', 'meter', 'no', 'riser', 'rising', 'main', 'air', 
# 'valve', 'sewer', 'stormwater', 'mushroom', 'Restoration'])

#*** toy program to check how the speller correction works ***
# misspelled = spell.unknown(['ferrule', 'ferrulle', 'vaced']) 
# for word in misspelled:
#     print(word,spell.correction(word)) 

import copy

def check_spellings(text):
    words = copy.deepcopy(text)
    punctuations = '''!()-[]{};:'"\,<>./?@#$%^&*_~='''
    for x in punctuations: 
        words = words.replace(x, " ")

    #split text to get a string of words
    words = words.split()

    #find all words that are misspelled
    misspelled = spell.unknown(words)
    if misspelled is None:
        return text

    for misspelled_word in misspelled:
        contains_digit = any(map(str.isdigit, misspelled_word))
        if contains_digit:
            continue

        corrected_word = spell.correction(misspelled_word)
        print("Corrected", misspelled_word, "with", corrected_word)
        text = text.replace(misspelled_word, corrected_word)

    return text


    