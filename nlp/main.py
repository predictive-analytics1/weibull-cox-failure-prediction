import UnityWaterWaterMainPredictor
import readingxl
import json
import checkspelling
data_source = "C:\\Users\\camera\\Desktop\\UnityWater\\Data Review Tool Monthly Update (A6034321).xlsm"
log_notes = readingxl.ReadLogNotes(data_source)

predictor = UnityWaterWaterMainPredictor.Predictor("bag_of_words.json")
for pwo in log_notes:
    log_note = log_notes[pwo].lower()

    # Correct simple spelling mistakes so we can keep the
    # bag of words to a managable length
    checked_log_notes = checkspelling.check_spellings(log_note)
    # print (checked_log_notes)

    # Run the predictor
    confidence = predictor.Predict(checked_log_notes)

    # Here we output to std::out, however this would be the
    # entry point into us performing further actions
    output = {"PWO": pwo, "Prediction": confidence}
    print(json.dumps(output, indent=2))

