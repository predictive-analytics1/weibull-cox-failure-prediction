 import os
 cwd=os.getcwd()
 os.chdir("/Users/camera/Desktop/UnityWater")
 os.listdir(".")



 https://stackoverflow.com/questions/16888888/how-to-read-a-xlsx-file-using-the-pandas-library-in-ipython


     'Set worksheet variables
      Dim ws As Worksheet
      Dim wh As Worksheet
      Set ws = ActiveWorkbook.Worksheets("Support Sheet")
      Set wh = ActiveWorkbook.Worksheets("Data Review")

      Dim KeywordCount(1 To 4) As Integer
      Dim Log_Notes As String
      Dim srch As String

      iRow = 8      'Start row of log notes table
      pwoc = 2      'Column with parent work order data
      lnc = 15      'Column with log notes
      otc = 11      'Output column

     'Define the comments to be examined
      For j = iRow To wh.Cells(iRow, lnc - 1).End(xlDown).Row Step 1
        
         Erase KeywordCount
         KeywordTotal = 0
         KeywordMax = 0
         KeywordDifference = 0
         AffectedPropertiesFactor = 0
         Confidence = 0
        
         srch = ""
         Log_Notes = ""
        
        'Check if comment is instance of an already analysed PWO
         If wh.Cells(j, pwoc).Value <> "" Then
            
            'Concatenate log notes within a parent work order
             For jj = j + 1 To wh.Cells(iRow, lnc).End(xlDown).Row Step 1
                 If wh.Cells(jj, pwoc).Value <> "" Then
                     Exit For
                 End If
                 jjj = jj
             Next
            
             If jjj > j Then
                 For jj = j To jjj Step 1
                     Log_Notes = Log_Notes & " " & wh.Cells(jj, lnc).Value
                 Next
             Else
                 Log_Notes = wh.Cells(j, lnc).Value
             End If

     Here we implement the count term occurence in corpus function:
     'Search a string for all instances of a substring
     Function KeywordFinder(Log_Notes As String, srch As String) As Integer
         init = 1
         Do
             pos = InStr(init, Log_Notes, srch, vbTextCompare)
             If pos > 0 Then
                 init = pos + Len(srch)
                 counter = counter + 1
             End If
         Loop While pos > 0
         KeywordFinder = counter
     End Function

         -- alternative impl --
         search_start = 0
         count = 0
         while True:
             next_term_start = corpus.find(term, search_start)
             if next_term_start > 0:
                 search_start += len(term)
                 count += 1
             else:
                 return count

            'Get frequency of keyword category
             For x = 1 To 4 Step 1
                 For i = 2 To ws.Cells(2, x + 3).End(xlDown).Row Step 1
                     srch = ws.Cells(i, x + 3).Value
                     KeywordCount(x) = KeywordCount(x) + KeywordFinder(Log_Notes, srch)
                 Next
             Next

            'Keywords Identified
             For x = 1 To 4 Step 1
                 KeywordTotal = KeywordTotal + KeywordCount(x)
             Next

            'Difference between WaterMain Keywords and Max Category (non-WaterMain)
             For x = 2 To 4 Step 1
                 If KeywordCount(x) > KeyworkMax Then
                     KeywordMax = KeywordCount(x)
                 End If
             Next
             KeywordDifference = KeywordCount(1) - KeywordMax
                'Assign a Actual Water Main confidence rating
             Confidence = ((2 * KeywordDifference * KeywordTotal) + (16 * KeywordDifference) + 50) / 100

     Confidence = max(min(Confidence, 1), 0)

             If Confidence > 1 Then
                 Confidence = 1
             ElseIf Confidence < 0 Then
                 Confidence = 0
             End If
                
             wh.Cells(j, otc).Value = Confidence
        
         End If
        
     Next

     End Sub