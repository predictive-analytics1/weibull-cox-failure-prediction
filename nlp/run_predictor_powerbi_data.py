import UnityWaterWaterMainPredictor
import json
import pandas as pd  
import numpy as np 

predictor = UnityWaterWaterMainPredictor.Predictor("bag_of_words.json")

uw_data=pd.read_json('C:\\Users\\camera\\Desktop\\UnityWater\\data.json')

is_main = uw_data['Asset_Classification_Code'] == 1000808

is_fn=uw_data['Misclassificaton_Type'] == 'FN'
is_fp=uw_data['Misclassificaton_Type'] == 'FP'
is_fpr=uw_data['Misclassificaton_Type'] == 'FPR'
#in the current classification when initially it is main and fp, assest classification code is assigned to 
#an arbitrary number for non-water-main 
#false positive water main - turned out to be non water main
uw_data.Asset_Classification_Code[is_main & is_fp]=1000806
#false positive non water main - turned out to be a water main
uw_data.Asset_Classification_Code[(~ is_main) & is_fn]=1000808
uw_data.Asset_Classification_Code[is_fpr]=0

#update the notes
is_main = uw_data['Asset_Classification_Code'] == 1000808

water_main_notes = uw_data.Work_Order_Log_Notes[is_main]
not_water_main_notes = uw_data.Work_Order_Log_Notes[~ is_main & ~ is_fpr]


row_has_a_label = ~pd.isna(uw_data.Asset_Classification_Code)

row_has_a_note = ~pd.isna(uw_data.Work_Order_Log_Notes)

mixed_uw_data = uw_data[~is_fpr & row_has_a_label & row_has_a_note]
# Add a column to the dataset for the new conficdedce or precitions value
# so that this can be used later for the metrics analsysis
# Predicted_Asset_Classification=[]
uw_data["Predicted_Asset_Classification"] = 0
for index, row in mixed_uw_data.iterrows():
    log_note=row.Work_Order_Log_Notes

    confidence = predictor.Predict(log_note)


    if confidence > 0.5:
        uw_data.Predicted_Asset_Classification[index] = 1000808
        

# run classification socre
# https://scikit-learn.org/stable/modules/generated/sklearn.metrics.classification_report.html
from sklearn import metrics
#create a copy of the dataset for modification of the labels
copy_uw_data=uw_data

#set all water main to 1 in both asset classificat
# ion code and predicted asset class
copy_uw_data.Asset_Classification_Code[is_main]=1

is_main_predicted = copy_uw_data.Predicted_Asset_Classification == 1000808
copy_uw_data.Predicted_Asset_Classification[is_main_predicted]=1

#set all labels for not water main to 0
copy_uw_data.Asset_Classification_Code[~is_main]=0

keep_mask = ~is_fpr & row_has_a_label & row_has_a_note


ground_truth=copy_uw_data.Asset_Classification_Code[keep_mask].astype('int')
predicted_label=copy_uw_data.Predicted_Asset_Classification[keep_mask].astype('int')

print(metrics.classification_report(ground_truth,predicted_label))
# truth_labesls

print("DO NOT REMOVE ME")