import openpyxl
import pandas as pd 
import numpy as np

def ReadLogNotes(excel_file):
    excel_sheet_name="Data Review"
    excel_log_column_name="WO Log Notes"
    excel_pwo_column_name="PWO"
    skip_header_rows=6
    
    excel_sheet = pd.read_excel(excel_file, sheet_name=excel_sheet_name, skiprows=skip_header_rows)

    # Format: PWO, LogNotes
    log_notes = {}
    for idx, row in excel_sheet.iterrows():
        pwo = row[excel_pwo_column_name]
        if np.isnan(pwo):
            continue
        if int(pwo) in log_notes:
            log_notes[int(pwo)] += " " + row[excel_log_column_name]
        else:
            log_notes[int(pwo)] = row[excel_log_column_name]

    return log_notes
