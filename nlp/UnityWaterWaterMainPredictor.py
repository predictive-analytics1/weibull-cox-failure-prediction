import json

class Predictor:

    def __init__(self, config_file_path):
        with open(config_file_path, "r") as fp:
            config_file = json.load(fp)

        self.KeywordCategories = config_file["KeywordCategories"]
        self.NonWaterMainCategories = config_file["NonWaterMainCategories"]
        self.WaterMainCategory = config_file["WaterMainCategory"]

    def KeywordFinder(self, corpus, term):
        return corpus.count(term)

    def Predict(self, log_notes):
        log_notes_i = log_notes.lower()

        # The following counts the occurance of all keywords in each category
        KeywordCount = {} # empty dict, to be filled with keyword count
        for category in self.KeywordCategories:
            KeywordCount[category] = 0
            keywords_i = [keyword.lower() for keyword in self.KeywordCategories[category]]
            for keyword in keywords_i:
                KeywordCount[category] += self.KeywordFinder(log_notes_i, keyword)   

        KeywordTotal = 0    
        for category in KeywordCount:
            KeywordTotal += KeywordCount[category]

        KeywordMax = max(KeywordCount[category] for category in self.NonWaterMainCategories)
        KeywordDifference = KeywordCount[self.WaterMainCategory] - KeywordMax    
        Confidence = ((2 * KeywordDifference * KeywordTotal) + (16 * KeywordDifference) + 50) / 100

        if Confidence > 1:
            Confidence = 1
        if Confidence < 0:
            Confidence = 0

        return Confidence
