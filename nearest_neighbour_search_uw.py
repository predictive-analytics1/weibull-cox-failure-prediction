#if the predictor changes the value of the water_main/not_water main --> reassign the assest
#by extracting the nearest asset to the given coordinate
import pandas as pd 
from pandas import DataFrame
import numpy as np
from scipy import spatial

#code used to convert an excel file to pickle
# df = pd.read_excel('XYCoordinates.xlsx',
#     sheet_name="wMains",
#     na_filter=False,
#     convert_float=False,
#     index_col=False)
# df.to_pickle('water_asset_coordinates.pkl')

services_excel_file='service_asset_coordinates.pkl'
wServices_sheet=pd.read_pickle(services_excel_file)
water_excel_file='water_asset_coordinates.pkl'
wMains_sheet=pd.read_pickle(water_excel_file)

wServices_kd_input = np.array([wServices_sheet.POINT_X.values, wServices_sheet.POINT_Y.values]).transpose()
wServices_tree = spatial.KDTree(wServices_kd_input)
wMains_kd_input = np.array([wMains_sheet.POINT_X.values, wMains_sheet.POINT_Y.values]).transpose()
wMains_tree = spatial.KDTree(wMains_kd_input)


def water_services_transformer(asset_number):
        string = str(asset_number)
        if string in wMains_sheet['MXASSETNUM'].values:
            x=wMains_sheet.loc[wMains_sheet['MXASSETNUM'] == string, 'POINT_X']
            y=wMains_sheet.loc[wMains_sheet['MXASSETNUM'] == string, 'POINT_Y']
            result = wServices_tree.query([x.values[0],y.values[0]], 2)
            asset = wServices_sheet.MXASSETNUM[result[1][1]]
            return asset
        elif string in wServices_sheet['MXASSETNUM'].values:
            x=wServices_sheet.loc[wServices_sheet['MXASSETNUM'] == string, 'POINT_X']
            y=wServices_sheet.loc[wServices_sheet['MXASSETNUM'] == string, 'POINT_Y']
            result = wMains_tree.query([x.values[0],y.values[0]], 2)
            asset = wMains_sheet.MXASSETNUM[result[1][1]]
            return asset

#test with a random number
# water_services_transformer(5020178)
# print('done')
