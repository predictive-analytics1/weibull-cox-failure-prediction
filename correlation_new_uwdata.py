import pandas as pd  
import numpy as np 
import seaborn as sn
import matplotlib.pyplot as plt

uw_data=pd.read_pickle('data.pickle')
print(uw_data.columns)

new = uw_data.filter(['Length (m)'], axis=1)
new.plot(kind='box', subplots=True, layout=(3,3), sharex=False, sharey=False)
plt.show()