#https://github.com/Bolton-and-Menk-GIS/restapi
import os
os.environ['RESTAPI_USE_ARCPY'] = 'FALSE'

# now import restapi
import restapi

rest_url = 'https://services2.arcgis.com/tQg86iShPXJPWQWw/ArcGIS/rest/services'
#rest_url = 'https://services2.arcgis.com/tQg86iShPXJPWQWw/ArcGIS/rest/services/UWPublicAccessUTrackerLayers_PRE/FeatureServer'
ags = restapi.ArcServer(rest_url)


print('Number of services: {}'.format(len(ags.services)))

#url = 'https://services.arcgis.com/V6ZHFr6zdgNZuVG0/arcgis/rest/services/Hazards_Uptown_Charlotte/FeatureServer/0'
url='https://services2.arcgis.com/tQg86iShPXJPWQWw/ArcGIS/rest/services/UWPublicAccessWaterInfrastructureLayers/FeatureServer/10'
watermain = restapi.FeatureLayer(url)

print('done')

