
import numpy as np
import pandas as pd
from datetime import datetime
from pathlib import Path
import matplotlib.pyplot as plt
import statistics
from lifelines import WeibullAFTFitter, LogNormalAFTFitter, LogLogisticAFTFitter
from lifelines import CoxPHFitter

def CategoricalToNumerical(values : pd.Series):
    UNIQUE_VALUES = values.unique()
    VALUE_TO_NUMBER_MAPPING = {}

    # Setup the map catagorical
    for idx, v in enumerate(UNIQUE_VALUES):
        VALUE_TO_NUMBER_MAPPING[v] = idx

    # Actually map values
    out = []
    out_idx = []
    for pidx, v in values.iteritems():
        try:
            out.append(VALUE_TO_NUMBER_MAPPING[v])
            out_idx.append(pidx)
        except:
            pass


    return pd.Series(out, index=out_idx), VALUE_TO_NUMBER_MAPPING


#             DuctilityRating
# Concrete        0
# Steels          1
# Plastics        2
def GetDuctilityRating(material):
    if material == MATERAL_TO_NUMBER_MAPPING["AC"]:
        return 0
    elif material == MATERAL_TO_NUMBER_MAPPING["DI"]:
        return 0 # ductile iron concrete lined
    elif material == MATERAL_TO_NUMBER_MAPPING["UNK"]:
        return 0 # probably something wierd
    elif material == MATERAL_TO_NUMBER_MAPPING["CI"]:
        return 1 # cast iron
    elif material == MATERAL_TO_NUMBER_MAPPING["CU"]:
        return 1 # copper
    elif material == MATERAL_TO_NUMBER_MAPPING["SS316"]:
        return 1 # stainless steel 316
    elif material == MATERAL_TO_NUMBER_MAPPING["MS"]:
        return 1 # Mild steel
    elif material == MATERAL_TO_NUMBER_MAPPING["GI"]:
        return 1 # Gal. Iron
    elif material == MATERAL_TO_NUMBER_MAPPING["PE100"]:
        return 2 # poly
    elif material == MATERAL_TO_NUMBER_MAPPING["ABS"]:
        return 2 # plastic
    elif material == MATERAL_TO_NUMBER_MAPPING["PVCM"]:
        return 2
    elif material == MATERAL_TO_NUMBER_MAPPING["PVCO"]:
        return 2
    elif material == MATERAL_TO_NUMBER_MAPPING["PVCU"]:
        return 2
    elif material == MATERAL_TO_NUMBER_MAPPING["GRP"]:
        return 2 #fiberglass
    else:
        throw("unknown material")

def CreateEventFromAssetInformation(asset, TimeSinceLastEvent=0, NumPreviousEvents=0, EventAgeSinceInstalled=0, IsFailure=True):

    good = np.isfinite(asset["Length (m)"]) \
         & np.isfinite(asset["Diameter"]) \
         & np.isfinite(asset["DMA"]) \
         & np.isfinite(asset["Material"])

    if ~good:
        return None

    event = {
        "durations":  TimeSinceLastEvent.days,
        "CompoundFailures": NumPreviousEvents,
        "Diameter": asset["Diameter"],
        "Length (m)": asset["Length (m)"],
        "DMA": asset["DMA"],
        "Material": asset["Material"],
        "Parent Workorder": asset['Parent Workorder'],
        "AgeSinceInstalled": EventAgeSinceInstalled.days,
        "is_an_event": IsFailure,
        "DuctilityRating": GetDuctilityRating(asset["Material"])
    }

    return event

def CreateAssetsEventsDatabaseFromDataframe(dataframe):
    asset_events = {}
    for idx, row in dataframe.iterrows():
        if row["Asset Number"] not in asset_events:
            install_date = row["Install Date"]
            try:
                install_date = datetime.strptime(install_date[0:10], "%Y-%m-%d")
            except:
                continue

            if install_date.year > datetime.now().year:
                continue
            
            try:
                asset_events[row["Asset Number"]] = {
                    "install_date": install_date,
                    "pwos": [],
                    "events": [],
                    'Diameter': int(row["Diameter"]), 
                    'Length (m)': int(row["Length (m)"]), 
                    'DMA': row["DMA_Num"], 
                    'Material': row["Material_Num"],
                    'Parent Workorder':row["Parent Workorder"]
                }
            except:
                continue
            
        
        # Check if the current parent work order has been seen  before for this asset
        if row["Parent Workorder"] in asset_events[row["Asset Number"]]["pwos"]:
            continue
        else:
            asset_events[row["Asset Number"]]["pwos"].append(row["Parent Workorder"])
        
        # Add event to the list of event this asset has experienced
        asset_events[row["Asset Number"]]["events"].append({
            "pwo": row["Parent Workorder"],
            "date": datetime.strptime(row["Calendar_Date"][0:10], "%Y-%m-%d"),
            "event_idx": idx
        })

    return asset_events

def CreateEventVectorFromAssetInformation(asset, RightCensoredOnly=False, RightCensoredAt=datetime.now()):
    event_vector = []

    events = [e["date"] for e in asset["events"]]
    num_failures = 1

    for idx in range(0, len(events)):
        if events[idx] > RightCensoredAt:
            throw("whoops")


    if len(events) > 1 and not RightCensoredOnly:
        sorted_events = sorted(events)
        event_deltas = [{
            "delta": sorted_events[i] - sorted_events[i-1],
            "age" : sorted_events[i] - asset["install_date"]
         } for i in range(1,len(sorted_events))]

        offset_next = None
        for event_delta in event_deltas:
            delta = event_delta["delta"]

            if offset_next is not None:
                delta += offset_next
                offset_next = None

            if delta.days < 30:
                offset_next = delta
                continue

            ev = CreateEventFromAssetInformation(
                asset,
                TimeSinceLastEvent = delta,
                NumPreviousEvents = num_failures,
                EventAgeSinceInstalled = event_delta["age"],
                IsFailure = True
            )

            if ev is None:
                continue

            event_vector.append(ev)
            num_failures += 1

    ev = CreateEventFromAssetInformation(
        asset,
        TimeSinceLastEvent = datetime.now() - asset["events"][-1]["date"],
        NumPreviousEvents = num_failures,
        EventAgeSinceInstalled = RightCensoredAt - asset["install_date"],
        IsFailure = False
    )

    if not ev is None:
        event_vector.append(ev)

    return event_vector

def CreateEventVectorFromAssetEvents(asset_events):
    all_events = []
    for assetid in asset_events:
        events = CreateEventVectorFromAssetInformation(asset_events[assetid])

        if any([ev is None for ev in events]):
            CreateEventVectorFromAssetInformation(asset_events[assetid])
            print(f"Somehow became none")
            continue

        all_events.extend(events)

    return all_events


def GetFailureProbability(P, f, end, start=0):
    return 100 - 100*P.predict_survival_function(f, times=end, conditional_after=start).iloc[0].to_numpy()[0]

def GetCumulativeFailureProbability(P, f, end):
    return 100 - 100*P.predict_survival_function(f, times=end).iloc[0].to_numpy()[0]

### Load data from File ###################################
#
###########################################################

uw_data = pd.read_pickle("data.pickle")

### Build data filters ####################################
# Because the dataset contains many different asset types #
# and different failure modes we make life simple by      #
# creating boolean vectors that we can then combine to    #
# grab subsets of the data                                #
###########################################################

ASSET_ID_MAIN = 1000806

is_uwi_impacted = uw_data["UWI Impacted"] >= 1
is_broken = uw_data["Problem_Code"] == "BROKENPIPE"
is_leak = uw_data["Problem_Code"] == "LEAK"
is_rm_work_order = uw_data["Work_Order_Type_Code"] == "RM"
is_pipe = uw_data.Asset_Classification_Code == ASSET_ID_MAIN


### Convert categorical to numerical ######################
# We have some data that is categorical that we wish to   #
# use in the model, this needs to be convered to being    #
# numerical.                                              #
###########################################################

uw_data["DMA_Num"], DMA_TO_NUMBER_MAPPING = CategoricalToNumerical(uw_data['DMA'])
uw_data["Material_Num"], MATERAL_TO_NUMBER_MAPPING = CategoricalToNumerical(uw_data['Material'])

### Subset data to failed pipes for training ##############
#
###########################################################

sel = (is_broken | is_leak) & (is_uwi_impacted) & is_rm_work_order & is_pipe
raw_failed_pipes         = pd.DataFrame(uw_data[sel].copy())
assetdb_failed_pipes     = CreateAssetsEventsDatabaseFromDataframe(raw_failed_pipes)
eventvec_failed_pipes    = CreateEventVectorFromAssetEvents(assetdb_failed_pipes)
trainingset_failed_pipes = pd.DataFrame(data=eventvec_failed_pipes)

# we don't need these variables any more
del raw_failed_pipes
del assetdb_failed_pipes
del eventvec_failed_pipes

### Train Model ###########################################
# Here we train the model using only data that includes   #
# failures. We have chosen to use the wiebull accelerated #
# failure model.                                          #
###########################################################

del trainingset_failed_pipes["Material"] # remove material, doesn't appear useful

aft = WeibullAFTFitter(penalizer=0.01)
aft.fit(trainingset_failed_pipes, duration_col='durations', event_col='is_an_event')
aft.print_summary()

# remove the transient variable?
del trainingset_failed_pipes

### Evaluate perf. of all assets ##########################
#
###########################################################

sel = is_pipe
all_pipes          = pd.DataFrame(uw_data[sel].copy())
assetdb_all_pipes  = CreateAssetsEventsDatabaseFromDataframe(all_pipes)

result = []
for idx, asset_id in enumerate(assetdb_all_pipes):
    ev = CreateEventVectorFromAssetInformation(assetdb_all_pipes[asset_id],
        RightCensoredOnly=True, RightCensoredAt=datetime(2020, 7, 1, 0, 0, 0))

    # some assets are just invalid
    if len(ev) == 0:
        continue

    le = ev[-1]
    le["pdindex"] = 0
    le = pd.DataFrame(le, index=["pdindex"])

    data = {
        "Cumulative Hazard Rate": GetCumulativeFailureProbability(aft, le, le["durations"]),
        "Asset ID": asset_id
    }

    year=[]
    year_start=2020
    days_since_last_failure = le["durations"]["pdindex"]
    for i in range(20):
        day_window_start = days_since_last_failure # + i * 365
        day_window_end = days_since_last_failure + (i + 1) * 365
        year_window_start = year_start + i
        year_window_end = year_start + i + 1

        year_ikey = "{}-{}".format(str(year_window_start), str(year_window_end))
        # year_fprob = GetFailureProbability(aft, le,
        #     start=day_window_start, end=day_window_end)
        year_fprob = GetCumulativeFailureProbability(aft, le, day_window_end)

        data[year_ikey] = year_fprob

    result.append(data)



result = pd.DataFrame(data=result)
result.set_index('assetid', inplace=True)
result_sorted = result.sort_values(by='st_year', ascending=False)

bad = result_sorted[result_sorted["cumu"] > 20]

for assetid, row in bad.iterrows():
    print(f"Asset {assetid}: "+
    f"Has a {round(row.cumu,2)}% chance of failing at this time, "+
    f"and has a {round(row.st_year,2)}% chance of failing through the next 1 year")

print ('done---')
