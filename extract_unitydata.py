import pandas as pd  
import numpy as np 

# POWERBI LARGE DATA EXTRACTION WITH PANDAS - to extract large dataset from PowerBi - 
# click on Python visual and run in Python script editor dataset.to_pickle('C:\\Users\\camera\\Desktop\\UnityWater\\data.pickle') 
uw_data=pd.read_pickle('data.pickle')


#filter data to include only Work_Order_Type_Code with a value RM
#then assign it to the name uw_data for the ease of use
# uw_data = downloaded_data.query("Work_Order_Type_Code=='RM'")

# uw_dictionary={'water_main':[],'not_watermain':[]}
# uw_dictionary['water_main'].extend(uw_data.Work_Order_Log_Notes)
# uw_dictionary['not_watermain'].extend(uw_data.Work_Order_Log_Notes)

# df=pd.DataFrame(uw_dictionary)

# print((uw_data.Misclassificaton_Type).uniqu5e())

#print ('1000806' in uw_data.Asset_Classification_Code)
#print (uw_data['Work_Order_Log_Notes'].where(uw_data['Asset_Classification_Code'] == '1000806'))
#code 1000806 corresponds with water_main; code 1000808 corresponds with water_service
is_main = uw_data['Asset_Classification_Code'] == 1000806

is_fn=uw_data['Misclassificaton_Type'] == 'FN'
is_fp=uw_data['Misclassificaton_Type'] == 'FP'
is_fpr=uw_data['Misclassificaton_Type'] == 'FPR'

#in the current classification when initially it is main and fp, assest classification code is assigned to 
#an arbitrary number for non-water-main 
#false positive water main - turned out to be non water main; randomly assign to water_service 
uw_data.Asset_Classification_Code[is_main & is_fp]=1000808
#false positive non water main - turned out to be a water main
uw_data.Asset_Classification_Code[(~ is_main) & is_fn]=1000806
uw_data.Asset_Classification_Code[is_fpr]=0

#update the notes
is_main = uw_data['Asset_Classification_Code'] == 1000806

water_main_notes = uw_data.Work_Order_Log_Notes[is_main]
not_water_main_notes = uw_data.Work_Order_Log_Notes[~ is_main & ~ is_fpr]

# cleaned_not_water_main = [x for x in not_water_main_notes if str(x) != 'nan']
# cleaned_water_main = [x for x in water_main_notes if str(x) != 'nan']

from pathlib import Path
ds_folder = Path.cwd()
wm_folder = ds_folder / "water_main"
wm_folder.mkdir(exist_ok=True)
nwm_folder = ds_folder / "non_water_main"
nwm_folder.mkdir(exist_ok=True)

offset = 1 
# for index, note in water_main_notes.iteritems():
#     if not isinstance(note, str):
#         continue
#     with (wm_folder / str(index)).open('w') as fp:
#         fp.write(note)
for offset, note in water_main_notes.iteritems():
    if not isinstance(note, str):
        continue
    with (wm_folder / str(offset)).open('w') as fp:
        fp.write(note)
        offset+=1
    
for offset, note in not_water_main_notes.iteritems():
    if not isinstance(note, str):
        continue
    with (nwm_folder / str(offset)).open('w') as fp:
        fp.write(note)
        offset+=1


