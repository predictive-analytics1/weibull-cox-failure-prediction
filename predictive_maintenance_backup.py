
import numpy as np
import pandas as pd
from datetime import datetime
from pathlib import Path
import matplotlib.pyplot as plt
import statistics

uw_data = pd.read_pickle("data.pickle")
# uw_data.to_csv('unitywaterdatacsvfile.csv')


ASSET_ID_MAIN = 1000806

is_uwi_impacted = uw_data["UWI Impacted"] >= 1
is_broken = uw_data["Problem_Code"] == "BROKENPIPE"
is_leak = uw_data["Problem_Code"] == "LEAK"
is_rm_work_order = uw_data["Work_Order_Type_Code"] == "RM"

is_pipe = uw_data.Asset_Classification_Code == ASSET_ID_MAIN

sel = (is_broken | is_leak) & (is_uwi_impacted) & is_rm_work_order & is_pipe

uw_pipes = pd.DataFrame(uw_data[sel].copy())



def CategoricalToNumerical(values : pd.Series):
    UNIQUE_VALUES = values.unique()
    VALUE_TO_NUMBER_MAPPING = {}

    # Setup the map catagorical
    for idx, v in enumerate(UNIQUE_VALUES):
        VALUE_TO_NUMBER_MAPPING[v] = idx

    # Actually map values
    out = []
    out_idx = []
    for pidx, v in values.iteritems():
        try:
            out.append(VALUE_TO_NUMBER_MAPPING[v])
            out_idx.append(pidx)
        except:
            pass


    return pd.Series(out, index=out_idx), VALUE_TO_NUMBER_MAPPING

uw_pipes["DMA_Num"], DMA_TO_NUMBER_MAPPING = CategoricalToNumerical(uw_pipes['DMA'])
uw_pipes["Material_Num"], MATERAL_TO_NUMBER_MAPPING = CategoricalToNumerical(uw_pipes['Material'])


# Create a DB of all assets
asset_events = {}
for idx, row in uw_pipes.iterrows():
    if row["Asset Number"] not in asset_events:
        install_date = row["Install Date"]
        try:
            install_date = datetime.strptime(install_date[0:10], "%Y-%m-%d")
        except:
            continue

        if install_date.year > datetime.now().year:
            continue
        
        try:
            asset_events[row["Asset Number"]] = {
                "install_date": install_date,
                "pwos": [],
                "events": [],
                'Diameter': int(row["Diameter"]), 
                'Length (m)': int(row["Length (m)"]), 
                'DMA': row["DMA_Num"], 
                'Material': row["Material_Num"],
                'Parent Workorder':row["Parent Workorder"]
            }
        except:
            continue
        
    
    # Check if the current parent work order has been seen  before for this asset
    if row["Parent Workorder"] in asset_events[row["Asset Number"]]["pwos"]:
        continue
    else:
        asset_events[row["Asset Number"]]["pwos"].append(row["Parent Workorder"])
    
    # Add event to the list of event this asset has experienced
    asset_events[row["Asset Number"]]["events"].append({
        "pwo": row["Parent Workorder"],
        "date": datetime.strptime(row["Calendar_Date"][0:10], "%Y-%m-%d"),
        "event_idx": idx
    })

#             DuctilityRating
# Concrete        0
# Steels          1
# Plastics        2
def GetDuctilityRating(material):
    if material == MATERAL_TO_NUMBER_MAPPING["AC"]:
        return 0
    elif material == MATERAL_TO_NUMBER_MAPPING["DI"]:
        return 0 # ductile iron concrete lined
    elif material == MATERAL_TO_NUMBER_MAPPING["UNK"]:
        return 0 # probably something wierd
    elif material == MATERAL_TO_NUMBER_MAPPING["CI"]:
        return 1 # cast iron
    elif material == MATERAL_TO_NUMBER_MAPPING["CU"]:
        return 1 # copper
    elif material == MATERAL_TO_NUMBER_MAPPING["MS"]:
        return 1 # Mild steel
    elif material == MATERAL_TO_NUMBER_MAPPING["PE100"]:
        return 2 # poly
    elif material == MATERAL_TO_NUMBER_MAPPING["PVCM"]:
        return 2
    elif material == MATERAL_TO_NUMBER_MAPPING["PVCO"]:
        return 2
    elif material == MATERAL_TO_NUMBER_MAPPING["PVCU"]:
        return 2

def CreateEventFromAssetInformation(asset, TimeSinceLastEvent=0, NumPreviousEvents=0, EventAgeSinceInstalled=0, IsFailure=True):

    good = np.isfinite(asset["Length (m)"]) \
         & np.isfinite(asset["Diameter"]) \
         & np.isfinite(asset["DMA"]) \
         & np.isfinite(asset["Material"])

    if ~good:
        return None

    event = {
        "durations":  TimeSinceLastEvent.days,
        "CompoundFailures": NumPreviousEvents,
        "Diameter": asset["Diameter"],
        "Length (m)": asset["Length (m)"],
        "DMA": asset["DMA"],
        "Material": asset["Material"],
        "Parent Workorder": asset['Parent Workorder'],
        "AgeSinceInstalled": EventAgeSinceInstalled.days,
        "is_an_event": IsFailure,
        "DuctilityRating": GetDuctilityRating(asset["Material"])
    }

    return event

def CreateEventVectorFromAssetInformation(asset, RightCensoredOnly=False, RightCensoredAt=datetime.now()):
    event_vector = []

    events = [e["date"] for e in asset["events"]]
    num_failures = 1

    for idx in range(0, len(events)):
        if events[idx] > RightCensoredAt:
            throw("whoops")


    if len(events) > 1 and not RightCensoredOnly:
        sorted_events = sorted(events)
        event_deltas = [{
            "delta": sorted_events[i] - sorted_events[i-1],
            "age" : sorted_events[i] - asset["install_date"]
         } for i in range(1,len(sorted_events))]

        offset_next = None
        for event_delta in event_deltas:
            delta = event_delta["delta"]

            if offset_next is not None:
                delta += offset_next
                offset_next = None

            if delta.days < 30:
                offset_next = delta
                continue

            event_vector.append(CreateEventFromAssetInformation(
                asset,
                TimeSinceLastEvent = delta,
                NumPreviousEvents = num_failures,
                EventAgeSinceInstalled = event_delta["age"],
                IsFailure = True
            ))

            num_failures += 1

    event_vector.append(CreateEventFromAssetInformation(
        asset,
        TimeSinceLastEvent = datetime.now() - asset["events"][-1]["date"],
        NumPreviousEvents = num_failures,
        EventAgeSinceInstalled = RightCensoredAt - asset["install_date"],
        IsFailure = False
    ))

    return event_vector

def CreateEventVectorFromAssetEvents(asset_events):
    all_events = []
    for assetid in asset_events:
        events = CreateEventVectorFromAssetInformation(asset_events[assetid])
        all_events.extend(events)

    return all_events

all_events = CreateEventVectorFromAssetEvents(asset_events)

# all_compound_failures=ds_dict.get('CompoundFailures')
# frequency_mode=statistics.mode(all_compound_failures)


ds = pd.DataFrame(data=all_events)

# ds = pd.DataFrame(
#     np.array([Diameter, length]).transpose(),
#     columns=["Length (m)", "Diameter"].transpose()
# )


from lifelines import WeibullAFTFitter, LogNormalAFTFitter, LogLogisticAFTFitter
from lifelines import CoxPHFitter
# cph = CoxPHFitter(penalizer=0.1)
#aft = WeibullAFTFitter(penalizer=0.1)

aft = WeibullAFTFitter(penalizer=0.01)

# aft.fit(ds, duration_col='durations', event_col='is_an_event')
# ds_strata_material = ds.copy()
# ds_strata_material['material_strata'] = pd.cut(ds_strata_material['Material'], np.arange(0,10,1))

#ds_strata_material[['Material','material_strata']].head()

# ds_strata_material = ds_strata_material.drop('Material', axis=1)
# cph.fit(ds_strata_material, duration_col='durations', event_col='is_an_event',
#     strata=['material_strata', 'Diameter','Length (m)', 'DMA'])
#del ds["DMA"]

# ds["LengthBins"] = pd.cut(ds["Length (m)"], [0, 100, 150, 500, 50000], labels=False)
# del ds["Length (m)"]




del ds["Material"]

#del ds["Parent Workorder"]

#del ds["CompoundFailures"]
aft.fit(ds, duration_col='durations', event_col='is_an_event')
aft.print_summary()
# aft.check_assumptions(ds)
#cph.predict_median(ds)
# cph.plot()
# plt.show()

# plt.figure()
# aft.predict_median(ds)
# plt.figure(2)
# aft.plot()
# import matplotlib
# matplotlib.pyplot.figure(3)
# cph.baseline_survival_.plot()
#ds_s = ds.sort_values('T')

# plt.figure(4)
# p = aft.predict_survival_function(ds.iloc[800])
# c = cph.predict_survival_function(ds.iloc[801],times=5000)
# b = cph.predict_survival_function(ds.iloc[802],times=5000)
# a = cph.predict_survival_function(ds.iloc[803],times=5000)
#hazard is not a probability, should use 1-survival function to predict the prob of failure 
# h= cph.predict_cumulative_hazard(ds.iloc[500])

#plt.plot(p.index.to_numpy(), p.to_numpy())
# plt.plot(p.index.to_numpy(), p.to_numpy(),label='800')
# first_trim_num=p.loc[90.0]
# second_trim_num=p.loc[180.0]
# third_trim_num=p.loc[271.0]
# forth_trim_num=p.loc[360.0]
# prediction_asset={'First trimester prediction':first_trim_num, 'Second trimester prediction':
#  second_trim_num, 'Third trimester prediction':third_trim_num, 'Forth trimester prediction':
#  forth_trim_num}
# plt.plot(c.index.to_numpy(), c.to_numpy(),label='801')
# plt.plot(b.index.to_numpy(), b.to_numpy(),label='802')
# plt.plot(b.index.to_numpy(), a.to_numpy(),label='803')
# plt.legend()

# matplotlib.pyplot.hist(p.to_numpy())
# plt.show()

def GetFailureProbability(P, f, end, start=0):
    return 100 - 100*P.predict_survival_function(f, times=end, conditional_after=start).iloc[0].to_numpy()[0]

def GetCumulativeFailureProbability(P, f, end):
    return 100 - 100*P.predict_survival_function(f, times=end).iloc[0].to_numpy()[0]


t = [0, 90, 180, 270, 360, 365*2, 365*3, 365*4, 365*5, 365*6, 365*7, 365*8, 365*9, 365*10, 365*20, 365*30, 365*40]

# P(s <= t | t)
survival_at_t = [
    GetFailureProbability(aft, ds.iloc[800], t[i], t[i-1])
    for i in range(1, len(t))]

# P(s <= t)
cumu_at_t = [
    GetCumulativeFailureProbability(aft, ds.iloc[800], t[i])
    for i in range(1, len(t))]



cumu_at_t.insert(0, 1)

for idx, st in enumerate(survival_at_t):
    sur = st
    cumu = cumu_at_t[idx]
    print(f"From inital cumulative failure hazard of {round(cumu, 2)}% at day {round(t[idx],0)}, there is an additional {round(sur, 2)}% "+
    f"chance of failing between day {round(t[idx],0)} and day {round(t[idx + 1],0)}")

result = []

for idx, asset_id in enumerate(asset_events):
    ev = CreateEventVectorFromAssetInformation(asset_events[asset_id],
        RightCensoredOnly=True, RightCensoredAt=datetime(2020, 7, 1, 0, 0, 0))
    le = ev[-1]
    le["pdindex"] = 0
    le = pd.DataFrame(le, index=["pdindex"])

    data = {
        "Cumulative Hazard Rate": GetCumulativeFailureProbability(aft, le, le["durations"]),
        "Asset ID": asset_id
    }

    year=[]
    year_start=2020
    days_since_last_failure = le["durations"]["pdindex"]
    for i in range(20):
        day_window_start = days_since_last_failure # + i * 365
        day_window_end = days_since_last_failure + (i + 1) * 365
        year_window_start = year_start + i
        year_window_end = year_start + i + 1

        year_ikey = "{}-{}".format(str(year_window_start), str(year_window_end))
        # year_fprob = GetFailureProbability(aft, le,
        #     start=day_window_start, end=day_window_end)
        year_fprob = GetCumulativeFailureProbability(aft, le, day_window_end)

        data[year_ikey] = year_fprob

    result.append(data)



result = pd.DataFrame(data=result)
result.set_index('assetid', inplace=True)
result_sorted = result.sort_values(by='st_year', ascending=False)

bad = result_sorted[result_sorted["cumu"] > 20]

for assetid, row in bad.iterrows():
    print(f"Asset {assetid}: "+
    f"Has a {round(row.cumu,2)}% chance of failing at this time, "+
    f"and has a {round(row.st_year,2)}% chance of failing through the next 1 year")

print ('done---')

# from lifelines import CoxPHFitter
# cph = CoxPHFitter()
# cph.fit(ds, duration_col='durations', event_col='is_an_event')
# cph.print_summary()
# plt.figure(2)
# cph.plot()

# # matplotlib.pyplot.figure(3)
# # ctv.baseline_survival_.plot()
# # ds_s = ds.sort_values('T')

# plt.figure(4)
# p = cph.predict_survival_function(ds.iloc[1])
# plt.plot(p.index.to_numpy(), p.to_numpy())
# # matplotlib.pyplot.hist(p.to_numpy())
# plt.show()

# durations_np = np.array(durations)

# import matplotlib
# from lifelines import WeibullFitter
# wbf = WeibullFitter()
# """
# Note:
#  event_observed (numpy array or pd.Series, optional) – length n,
#   True if the the death was observed, False if the event was lost (right-censored).
#   Defaults all True if event_observed==None
# """

# wbf.fit(durations_np)
# print(wbf.lambda_)
# wbf.print_summary()

# wbf.plot()
# matplotlib.pyplot.show()

# wbf.plot_cumulative_density()
# matplotlib.pyplot.show()

# wbf.plot_cumulative_hazard()
# matplotlib.pyplot.show()

# from lifelines import WeibullAFTFitter
# aft = WeibullAFTFitter()
# df = pd.DataFrame({'T': durations})
# aft.fit(df, 'T')
# aft.print_summary()



