from sklearn.datasets import load_files
import sklearn.model_selection as model_selection
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split

# Load the text data
categories = [
    'non_water_main',
    'water_main',
]

text_train_subset = load_files('unitywaterdata',
    categories=categories, encoding='utf-8', decode_error='ignore')

split_category=text_train_subset.target
split_data=text_train_subset.data
train_data, test_data, train_category, test_category  = train_test_split(split_data, split_category, test_size = 0.20)

count_vect = CountVectorizer()
X_train_counts = count_vect.fit_transform(train_data)
X_test_counts = count_vect.fit_transform(test_data)

from sklearn.feature_extraction.text import TfidfTransformer

tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
X_test_tfidf = tfidf_transformer.fit_transform(X_test_counts)

from sklearn.naive_bayes import MultinomialNB
clf = MultinomialNB().fit(X_train_tfidf, train_category)

# docs_new = ['''Leak on the road traffic control vac truck leaking out of a old patch  in the road''', ''' tap repair ''']
# X_new_counts = count_vect.transform(docs_new)
# X_new_tfidf = tfidf_transformer.transform(X_new_counts)

# predicted = clf.predict(X_new_tfidf)

# for doc, category in zip(docs_new, predicted):
#     print('%r => %s' % (doc, text_train_subset.target_names[category]))


from sklearn.pipeline import Pipeline
text_clf = Pipeline([
    ('vect', CountVectorizer()),
    ('tfidf', TfidfTransformer()),
    ('clf', MultinomialNB()),
])

text_clf.fit(train_data, train_category)

import numpy as np


docs_test = test_data
predicted = text_clf.predict(docs_test)
print(np.mean(predicted == test_category))


from sklearn.linear_model import SGDClassifier
text_clf = Pipeline([
    ('vect', CountVectorizer()),
    ('tfidf', TfidfTransformer()),
    ('clf', SGDClassifier(loss='modified_huber', penalty='l2',
                          alpha=1e-3, eta0=0.2, learning_rate='adaptive'))
])

text_clf.fit(train_data, train_category)

predicted = text_clf.predict(docs_test)
print(np.mean(predicted == test_category))

# #test the SDG Classifier on the specified sentence
# clf = SGDClassifier().fit(X_train_tfidf, train_category)
# docs_new = ['''Leak on the road traffic control vac truck leaking out of a old patch  in the road''', ''' tap leaking house''']
# X_new_counts = count_vect.transform(docs_new)
# X_new_tfidf = tfidf_transformer.transform(X_new_counts)

# predicted = clf.predict(X_new_tfidf)
# for doc, category in zip(docs_new, predicted):
#     print('%r => %s' % (doc, text_train_subset.target_names[category]))


# docs_new = ['''Leak on the road traffic control vac truck leaking out of a old patch  in the road''', ''' tap repair ''']
# X_new_counts = count_vect.transform(docs_new)
# X_new_tfidf = tfidf_transformer.transform(X_new_counts)

# predicted = clf.predict(X_new_tfidf)

# for doc, category in zip(docs_new, predicted):
#     print('%r => %s' % (doc, text_train_subset.target_names[category]))
 
result = pd.DataFrame(data=result)
result.set_index('assetid', inplace=True)
result_sorted = result.sort_values(by='st_year', ascending=False)

bad = result_sorted[result_sorted["cumu"] > 20]
for assetid, row in bad.iterrows():
    print(f"Asset {assetid}: "+
    f"Has a {round(row.cumu,2)}% chance of failing at this time, "+
    f"and has a {round(row.st_year,2)}% chance of failing through the next 1 year")



print('done')




